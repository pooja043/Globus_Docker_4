�
�U�Wc           @   sT  d  Z  d d l Z d d l Z d d l m Z d d l m Z d d l m Z	 d d l m
 Z d d l m Z d Z d	 e f d
 �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d S(   s�  Parser for XML results returned by NCBI's Entrez Utilities.

This parser is used by the read() function in Bio.Entrez, and is not
intended be used directly.

The question is how to represent an XML file as Python objects. Some
XML files returned by NCBI look like lists, others look like dictionaries,
and others look like a mix of lists and dictionaries.

My approach is to classify each possible element in the XML as a plain
string, an integer, a list, a dictionary, or a structure. The latter is a
dictionary where the same key can occur multiple times; in Python, it is
represented as a dictionary where that key occurs once, pointing to a list
of values found in the XML file.

The parser then goes through the XML and creates the appropriate Python
object for each element. The different levels encountered in the XML are
preserved on the Python side. So a subelement of a subelement of an element
is a value in a dictionary that is stored in a list which is a value in
some other dictionary (or a value in a list which itself belongs to a list
which is a value in a dictionary, and so on). Attributes encountered in
the XML are stored as a dictionary in a member .attributes of each element,
and the tag name is saved in a member .tag.

To decide which kind of Python object corresponds to each element in the
XML, the parser analyzes the DTD referred at the top of (almost) every
XML file returned by the Entrez Utilities. This is preferred over a hand-
written solution, since the number of DTDs is rather large and their
contents may change over time. About half the code in this parser deals
wih parsing the DTD, and the other half with the XML itself.
i����N(   t   expat(   t   BytesIO(   t   urlopen(   t   urlparse(   t   unicodes   restructuredtext ent   IntegerElementc           B   s   e  Z d  �  Z RS(   c         C   sE   t  j |  � } y |  j } Wn t k
 r0 | SXd | t | � f S(   Ns!   IntegerElement(%s, attributes=%s)(   t   intt   __repr__t
   attributest   AttributeErrort   repr(   t   selft   textR   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   7   s    (   t   __name__t
   __module__R   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   6   s   t   StringElementc           B   s   e  Z d  �  Z RS(   c         C   sE   t  j |  � } y |  j } Wn t k
 r0 | SXd | t | � f S(   Ns    StringElement(%s, attributes=%s)(   t   strR   R   R	   R
   (   R   R   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   A   s    (   R   R   R   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   @   s   t   UnicodeElementc           B   s   e  Z d  �  Z RS(   c         C   sE   t  j |  � } y |  j } Wn t k
 r0 | SXd | t | � f S(   Ns!   UnicodeElement(%s, attributes=%s)(   R   R   R   R	   R
   (   R   R   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   K   s    (   R   R   R   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   J   s   t   ListElementc           B   s   e  Z d  �  Z RS(   c         C   sE   t  j |  � } y |  j } Wn t k
 r0 | SXd | t | � f S(   Ns   ListElement(%s, attributes=%s)(   t   listR   R   R	   R
   (   R   R   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   U   s    (   R   R   R   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   T   s   t   DictionaryElementc           B   s   e  Z d  �  Z RS(   c         C   sE   t  j |  � } y |  j } Wn t k
 r0 | SXd | t | � f S(   Ns   DictElement(%s, attributes=%s)(   t   dictR   R   R	   R
   (   R   R   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   _   s    (   R   R   R   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   ^   s   t   StructureElementc           B   s#   e  Z d  �  Z d �  Z d �  Z RS(   c         C   s>   t  j |  � x! | D] } t  j |  | g  � q W| |  _ d  S(   N(   R   t   __init__t   __setitem__t   listkeys(   R   t   keyst   key(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   l   s    c         C   s:   | |  j  k r# |  | j | � n t j |  | | � d  S(   N(   R   t   appendR   R   (   R   R   t   value(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   r   s    c         C   sE   t  j |  � } y |  j } Wn t k
 r0 | SXd | t | � f S(   Ns   DictElement(%s, attributes=%s)(   R   R   R   R	   R
   (   R   R   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   x   s    (   R   R   R   R   R   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   k   s   		t   NotXMLErrorc           B   s   e  Z d  �  Z d �  Z RS(   c         C   s   | |  _  d  S(   N(   t   msg(   R   t   message(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   �   s    c         C   s   d |  j  S(   NsZ   Failed to parse the XML data (%s). Please make sure that the input data are in XML format.(   R   (   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   __str__�   s    (   R   R   R   R!   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   �   s   	t   CorruptedXMLErrorc           B   s   e  Z d  �  Z d �  Z RS(   c         C   s   | |  _  d  S(   N(   R   (   R   R    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   �   s    c         C   s   d |  j  S(   NsZ   Failed to parse the XML data (%s). Please make sure that the input data are not corrupted.(   R   (   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR!   �   s    (   R   R   R   R!   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR"   �   s   	t   ValidationErrorc           B   s    e  Z d  Z d �  Z d �  Z RS(   s  Validating parsers raise this error if the parser finds a tag in the XML that is not defined in the DTD. Non-validating parsers do not raise this error. The Bio.Entrez.read and Bio.Entrez.parse functions use validating parsers by default (see those functions for more information)c         C   s   | |  _  d  S(   N(   t   name(   R   R$   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   �   s    c         C   s   d |  j  S(   Ns�   Failed to find tag '%s' in the DTD. To skip all tags that are not represented in the DTD, please call Bio.Entrez.read or Bio.Entrez.parse with validate=False.(   R$   (   R   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR!   �   s    (   R   R   t   __doc__R   R!   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR#   �   s   	t   DataHandlerc           B   sx  e  Z d  d l Z e j �  d k rE e j j e j d � d � Z n- e j j	 d � Z
 e j j e
 d d � Z [
 e j j e d d d	 � Z [ [ y e j e � Wn. e k
 r� Z e j j e � s� e � q� n Xd  d
 l m Z e j j e e j d � d	 � Z [ d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z  RS(   i����Nt   Windowst   APPDATAt	   biopythont   ~s   .configt   Biot   Entrezt   DTDs(   R,   i    c         C   s�   g  |  _  g  |  _ g  |  _ g  |  _ g  |  _ g  |  _ i  |  _ g  |  _ g  |  _ | |  _	 t
 j d d � |  _ |  j j t
 j � |  j |  j _ d  S(   Nt   namespace_separatort    (   t   stackt   errorst   integerst   stringst   listst   dictionariest
   structurest   itemst   dtd_urlst
   validatingR    t   ParserCreatet   parsert   SetParamEntityParsingt   XML_PARAM_ENTITY_PARSING_ALWAYSt   xmlDeclHandlert   XmlDeclHandler(   R   t   validate(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR   �   s    										c         C   s�   | j  j d k r | j } n  t | d � rE | j rE t d � � n  y |  j j | � Wn= t j	 k
 r� } |  j j
 r� t | � � q� t | � � n Xy |  j SWn8 t k
 r� |  j j
 r� t d � � q� t d � � n Xd S(   s2   Set up the parser and let it parse the XML resultst   EvilHandleHackt   closeds   Can't parse a closed handles�   Failed to parse the XML file correctly, possibly due to a bug in Bio.Entrez. Please contact the Biopython developers at biopython-dev@biopython.org for assistance.s   XML declaration not foundN(   t	   __class__R   t   _handlet   hasattrRB   t   IOErrorR;   t	   ParseFileR    t
   ExpatErrort   StartElementHandlerR"   R   t   objectR	   t   RuntimeError(   R   t   handlet   e(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   read�   s     c         c   st  d } xgt  ro| j | � } | s� |  j r6 t � n  y x |  j D] } | VqC WWn8 t k
 r� |  j j r~ t d � � q� t	 d � � n X|  j j
 d t  � d  |  _ d  Sy |  j j
 | t � Wn= t j k
 r} |  j j r� t | � � qt	 | � � n X|  j sq	 n  |  j d } t | t � s?t d � � n  x* t | � d k rk| j d � } | VqBWq	 Wd  S(   Ni   s�   Failed to parse the XML file correctly, possibly due to a bug in Bio.Entrez. Please contact the Biopython developers at biopython-dev@biopython.org for assistance.s   XML declaration not foundt    i    sV   The XML file does not represent a list. Please use Entrez.read instead of Entrez.parsei   (   t   TrueRN   R0   R"   RJ   R	   R;   RI   RK   R   t   Parset   Nonet   FalseR    RH   t
   isinstanceR   t
   ValueErrort   lent   pop(   R   RL   t   BLOCKR   t   recordRM   t   records(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   parse�   s<    					c         C   sO   |  j  |  j _ |  j |  j _ |  j |  j _ |  j |  j _ |  j	 |  j _
 d  S(   N(   t   startElementHandlerR;   RI   t   endElementHandlert   EndElementHandlert   characterDataHandlert   CharacterDataHandlert   externalEntityRefHandlert   ExternalEntityRefHandlert   startNamespaceDeclHandlert   StartNamespaceDeclHandler(   R   t   versiont   encodingt
   standalone(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR>     s
    c         C   s   t  d � � d  S(   NsL   The Bio.Entrez parser cannot handle XML data that make use of XML namespaces(   t   NotImplementedError(   R   t   prefixt   un(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyRc   !  s    c         C   s�  d |  _  | |  j k r$ t �  } n4| |  j k r? t �  } n| |  j k rd t |  j | � } n� | |  j k rt | d � } | d =t | d � } | d =| d k r� t �  } nB | d k r� t d d g � } n! | d	 k r� t �  } n	 t	 �  } | | _
 | | _ nH | |  j |  j |  j k r:| |  _ d  S|  j rRt | � � n d } | d k r�| | _ | r�t | � | _ n  t |  j � d
 k r�|  j d } y | j | � Wq�t k
 r�| | | <q�Xq�n  |  j j | � d  S(   NRO   t   Namet   Typet	   Structuret
   ArticleIdst   Historyt   pubmedt   medlinet   Listi    i����(   Rn   Ro   (   t   contentR4   R   R5   R   R6   R   R7   R   R   t   itemnamet   itemtypeR3   R1   R2   R   R9   R#   t   tagR   RV   R0   R   R	   (   R   R$   t   attrsRJ   Ru   t   current(    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR\   $  sL    						c         C   s�  |  j  } | |  j k r7 | d k r( d  St | � � n| |  j k rU t | � } n� | |  j k r� y t | � } WqJt k
 r� t | � } qJXn� | |  j	 k r4|  j
 j �  |  _ |  j j d k r� d  S|  j j d k r� | r� t | � } n0 y t | � } Wn t k
 r$t | � } n X|  j j } n |  j
 j �  |  _ d  S| | _ |  j rwt |  j � | _ |  ` n  |  j
 d } | d k r�y | j | � Wq�t k
 r�| | | <q�Xn  d  S(   NRO   Rr   Rm   t   Integeri����(   s   Lists	   Structure(   Rs   R1   RK   R2   R   R3   R   t   UnicodeEncodeErrorR   R7   R0   RW   RJ   Ru   Rt   Rv   R   R   R   R	   (   R   R$   R   Rx   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR]   Q  sF    				c         C   s   |  j  | 7_  d  S(   N(   Rs   (   R   Rs   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR_   {  s    c      	      s$  | j  �  d k r& |  j j | � d S| d k r� | t j j t j j d t j j t j j	 d d f f f k r� |  j
 j | � d Sxl | d t j j t j j f k r� | d t j j	 t j j f k r� t | d � d k r� | d d } q� W| d t j j t j j f k r*|  j j | � d S| d t j j t j j f k r�| d t j j t j j f k r�|  j j | � d Sg  � g  � �  � � f d �  �  �  | � t � � d k r�t � � d k r�|  j j | � n< t � � d k r	|  j j | � n |  j j i � | 6� d S(	   s*  This callback function is called for each element declaration:
        <!ELEMENT       name          (...)>
        encountered in a DTD. The purpose of this function is to determine
        whether this element should be regarded as a string, integer, list
        dictionary, structure, or error.t   ERRORNt   Itemi    i   i   c            s�   |  d \ } } } | d  k r� | t j j t j j f k rb x= | D] } � j | d � qD Wq� x� | D] } �  | � qi Wnq | j �  d k r� | t j j t j j f k r� � j | � q� | t j j t j j f k r� � j | � q� n  d  S(   Ni   i   R{   (	   RR   R    t   modelt   XML_CQUANT_PLUSt   XML_CQUANT_REPR   t   uppert   XML_CQUANT_NONEt   XML_CQUANT_OPT(   R}   t
   quantifierR$   t   childrent   child(   t   countt   multiplet   single(    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR�   �  s    (    (   R�   R1   R   R    R}   t   XML_CTYPE_MIXEDR   RR   t   XML_CTYPE_NAMER�   R7   t   XML_CTYPE_SEQt   XML_CTYPE_CHOICER�   RV   t   XML_CTYPE_EMPTYR3   R~   R4   R5   R6   t   update(   R   R$   R}   (    (   R�   R�   R�   sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   elementDecl~  sH    		
$c         C   s�   t  j j t j | � } y t | d � } Wn t k
 r> n X| St  j j t j | � } y t | d � } Wn t k
 r� n X| Sd  S(   Nt   rb(	   t   ost   patht   joinR&   t   local_dtd_dirt   openRF   t   global_dtd_dirRR   (   R   t   filenameR�   RL   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   open_dtd_file�  s    c         C   sq   t  j j t j | � } y t | d � } Wn( t k
 rU t j d | | f � n X| j	 | � | j
 �  d  S(   Nt   wbs   Failed to save %s at %s(   R�   R�   R�   R&   R�   R�   RF   t   warningst   warnt   writet   close(   R   R�   R   R�   RL   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   save_dtd_file�  s    c         C   s~  t  | � } | d d k r% | } ng | d d k r� y |  j d } Wn t k
 r_ d } n Xt j j | � } | j d � d | } n  |  j j | � t j j | � \ } }	 |  j	 |	 � }
 |
 s8y t
 | � }
 Wn' t k
 rt d |	 | f � � n X|
 j �  } |
 j �  |  j |	 | � t | � }
 n  |  j j | � } |  j | _ | j |
 � |
 j �  |  j j �  d S(	   sl  The purpose of this function is to load the DTD locally, instead
        of downloading it from the URL specified in the XML. Using the local
        DTD results in much faster parsing. If the DTD is not found locally,
        we try to download it. If new DTDs become available from NCBI,
        putting them in Bio/Entrez/DTDs will allow the parser to see them.i    t   httpRO   i����s    http://www.ncbi.nlm.nih.gov/dtd/t   /s   Failed to access %s at %si   (   t	   _urlparseR8   t
   IndexErrorR�   R�   t   dirnamet   rstripR   t   splitR�   t   _urlopenRF   RK   RN   R�   R�   R   R;   t   ExternalEntityParserCreateR�   t   ElementDeclHandlerRG   RW   (   R   t   contextt   baset   systemIdt   publicIdt   urlinfot   urlt   sourcet   locationR�   RL   R   R;   (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyRa   �  s8    	


(!   R   R   t   platformt   systemR�   R�   R�   t   getenvt	   directoryt
   expandusert   homeR�   t   makedirst   OSErrort	   exceptiont   isdirR+   R,   R   t   __path__R�   R   RN   R[   R>   Rc   R\   R]   R_   R�   R�   R�   Ra   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyR&   �   s:   !"		#	4			-	*		O		
(   R%   R�   R�   t   xml.parsersR    t   ioR   t	   Bio._py3kR   R�   R   R�   R   t   __docformat__R   R   R   R   R   R   R   R   R   R   RU   R   R"   R#   RJ   R&   (    (    (    sa   /usr/local/lib/python2.7/dist-packages/biopython-1.65-py2.7-linux-x86_64.egg/Bio/Entrez/Parser.pyt   <module>$   s$   



	