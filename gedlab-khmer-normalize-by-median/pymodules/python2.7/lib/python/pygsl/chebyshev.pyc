�
�M�Uc           @   sL   d  Z  d d l Z d d l m Z d d l m Z d e f d �  �  YZ d S(   s�  
This module describes routines for computing Chebyshev
approximations to univariate functions.  A Chebyshev approximation is a
truncation of the series \M{f(x) = S{sum} c_n T_n(x)}, where the Chebyshev
polynomials \M{T_n(x) = cos(n rccos x)} provide an orthogonal basis of
polynomials on the interval [-1,1] with the weight function
\M{1 / sqrt{1-x^2}}.  The first few Chebyshev polynomials are, T_0(x) = 1,
T_1(x) = x, T_2(x) = 2 x^2 - 1.

def f(x, p):
    if x < 0.5:
        return 0.25
    else:
        return 0.75

     
n = 10000;

cs = cheb_series(40)
F = gsl_function(f, None)
cs.init(F, 0.0, 1.0)

nf = float(n)
for i in range(100):
    x = i / nf
    r10 = cs.eval_n(10, x)
    r40 = cs.eval(x)
    print "%g %g %g %g" % (x, f(x, None), r10, r40)
i����N(   t
   _workspace(   t   gsl_functiont   cheb_seriesc           B   s[  e  Z d  Z e j Z e j Z e j Z	 e j
 Z e j Z e j Z e j Z e j Z e j Z e j Z e j Z e j Z e j Z e j Z e j  Z! e j" Z# e j$ Z% e j& Z' e j( Z) d �  Z* d �  Z+ d �  Z, d �  Z- d �  Z. d �  Z/ d �  Z0 d �  Z1 d	 �  Z2 d
 �  Z3 d �  Z4 d �  Z5 d �  Z6 d �  Z7 d �  Z8 d �  Z9 d �  Z: d �  Z; RS(   so   
    This class manages all internal detail. It provides the space for a
    Chebyshev series of order N.

    c         C   s   | |  _  t j |  | � d S(   sF   
        input : n
        @params n : number of coefficients
        N(   t   _sizeR    t   __init__(   t   selft   size(    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyR   A   s    	c         C   s   |  j  |  j | j �  | | � S(   s�  
        This function computes the Chebyshev approximation for the
        function F over the range (a,b) to the previously specified order.
        The computation of the Chebyshev approximation is an \M{O(n^2)}
        process, and requires n function evaluations.

        input : f, a, b
        @params  f : a gsl_function
        @params  a : lower limit
        @params  b : upper limit
        (   t   _initt   _ptrt   get_ptr(   R   t   ft   at   b(    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   initI   s    c         C   s   |  j  |  j | � S(   s�   
        This function evaluates the Chebyshev series CS at a given point X

        input : x
             x ... value where the series shall be evaluated.
        (   t   _evalR   (   R   t   x(    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   evalW   s    c         C   s   |  j  |  j | � S(   sQ  
         This function computes the Chebyshev series  at a given point X,
         estimating both the series RESULT and its absolute error ABSERR.
         The error estimate is made from the first neglected term in the
         series.
         
         input : x
             x ... value where the error shall be evaluated.
        (   t	   _eval_errR   (   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   eval_err`   s    
c         C   s   |  j  |  j | | � S(   s  
         This function evaluates the Chebyshev series CS at a given point
         N, to (at most) the given order ORDER.

        input : n, x
             n ... number of cooefficients
             x ... value where the series shall be evaluated.
         
        (   t   _eval_nR   (   R   t   orderR   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   eval_nl   s    
c         C   s   |  j  |  j | | � S(   s�  
        This function evaluates a Chebyshev series CS at a given point X,
        estimating both the series RESULT and its absolute error ABSERR,
        to (at most) the given order ORDER.  The error estimate is made
        from the first neglected term in the series.

        input : n, x
             n ... number of cooefficients
             x ... value where the error shall be evaluated.

        (   t   _eval_n_errR   (   R   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt
   eval_n_errx   s    c         C   s)   t  |  j � } |  j | j |  j � | S(   s�   
        This method computes the derivative of the series CS. It returns
        a new instance of the cheb_series class.
        (   R   R   t   _calc_derivR   (   R   t   tmp(    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt
   calc_deriv�   s    c         C   s)   t  |  j � } |  j | j |  j � | S(   s�   
        This method computes the integral of the series CS. It returns
        a new instance of the cheb_series class.

        (   R   R   t   _calc_integR   (   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt
   calc_integ�   s    c         C   s   |  j  |  j � S(   s2   
        Get the chebyshev coefficients. 
        (   t
   _get_coeffR   (   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   get_coefficients�   s    c         C   s   |  j  |  j | � S(   s3   
        Sets the chebyshev coefficients. 
        (   t
   _set_coeffR   (   R   t   coefs(    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   set_coefficients�   s    c         C   s   |  j  |  j � S(   sF   
        Get the lower boundary of the current representation
        (   t   _get_aR   (   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   get_a�   s    c         C   s   |  j  |  j | � S(   sF   
        Set the lower boundary of the current representation
        (   t   _set_aR   (   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   set_a�   s    c         C   s   |  j  |  j � S(   sF   
        Get the upper boundary of the current representation
        (   t   _get_bR   (   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   get_b�   s    c         C   s   |  j  |  j | � S(   sF   
        Set the upper boundary of the current representation
        (   t   _set_bR   (   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   set_b�   s    c         C   s   |  j  |  j � S(   sk   
        Get the value f (what is it ?) The documentation does not tell anything
        about it.
        (   t   _get_fR   (   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   get_f�   s    c         C   s   |  j  |  j | � S(   s0   
        Set the value f (what is it ?)
        (   t   _set_fR   (   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   set_f�   s    c         C   s   |  j  |  j � S(   sk   
        Get the value f (what is it ?) The documentation does not tell anything
        about it.
        (   t   _get_order_spR   (   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   get_order_sp�   s    c         C   s   |  j  |  j | � S(   s0   
        Set the value f (what is it ?)
        (   t   _set_order_spR   (   R   R   (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   set_order_sp�   s    (<   t   __name__t
   __module__t   __doc__t	   _callbackt   gsl_cheb_alloct   _alloct   gsl_cheb_freet   _freet   gsl_cheb_initR   t   gsl_cheb_evalR   t   gsl_cheb_eval_errR   t   gsl_cheb_eval_nR   t   gsl_cheb_eval_n_errR   t   gsl_cheb_calc_derivR   t   gsl_cheb_calc_integR   t   pygsl_cheb_get_coefficientsR   t   pygsl_cheb_set_coefficientsR   t   pygsl_cheb_get_aR"   t   pygsl_cheb_set_aR$   t   pygsl_cheb_get_bR&   t   pygsl_cheb_set_bR(   t   pygsl_cheb_get_fR*   t   pygsl_cheb_set_fR,   t   pygsl_cheb_get_order_spR.   t   pygsl_cheb_set_order_spR0   R   R   R   R   R   R   R   R   R   R!   R#   R%   R'   R)   R+   R-   R/   R1   (    (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyR   %   sL   																													
									(   R4   R5   t   _generic_solverR    R   R   (    (    (    sI   /mnt/galaxyTools/tools/pymodules/python2.7//lib/python/pygsl/chebyshev.pyt   <module>   s   