#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'pyDNase==0.2.3','wellington_footprints.py'
__requires__ = 'pyDNase==0.2.3'
import pkg_resources
pkg_resources.run_script('pyDNase==0.2.3', 'wellington_footprints.py')
