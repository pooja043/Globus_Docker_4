#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'bottle==0.12.7','bottle.py'
__requires__ = 'bottle==0.12.7'
import pkg_resources
pkg_resources.run_script('bottle==0.12.7', 'bottle.py')
