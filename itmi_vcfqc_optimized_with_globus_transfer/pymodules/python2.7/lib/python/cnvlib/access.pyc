ó
v`¡Vc           @   s}   d  Z  d d l Z d d l Z d d l Z d d l m Z d   Z d   Z d   Z	 d   Z
 d   Z d	   Z d
   Z d S(   s'  List the locations of accessible sequence regions in a FASTA file.

Inaccessible regions, e.g. telomeres and centromeres, are masked out with N in
the reference genome sequence; this script scans those to identify the
coordinates of the accessible regions (those between the long spans of N's).
iÿÿÿÿN(   t   parse_regionsc         C   s*   t  j d |  | | | |  |  | | f S(   s2   Log a coordinate range, then return it as a tuple.s%   	Accessible region %s:%d-%d (size %d)(   t   loggingt   info(   t   chromt	   run_startt   run_end(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   log_this   s    	c         c   so  t  |   ]} d
 } } } x%| D]} | j d  r | d
 k	 rY t | | |  Vn  | j d
 d  d d } d
 } d } t j d |  q$ | j   } d | k rt d   | D  ré | d
 k	 rt | | |  Vd
 } qq1t	 j
 | d d } t	 j | d k  d } | d
 k	 r?t | | | | d  Vn, | d d k rkt | | | | d  Vn  t	 j |  d k } | j   rä| | d | }	 | d | | }
 x1 t |	 |
  D] \ } } t | | |  VqÀWn  | d	 d t |  k  r| | d	 d } q1d
 } n | d
 k r1| } n  | t |  7} q$ W| d
 k	 ret | | |  Vn  Wd
 QXd
 S(   sA   Find accessible sequence regions (those not masked out with 'N').t   >i   i    s#   %s: Scanning for accessible regionst   Nc         s   s   |  ] } | d  k Vq d S(   R   N(    (   t   .0t   c(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pys	   <genexpr>*   s    t   dtypeR
   iÿÿÿÿN(   t   opent   Nonet
   startswithR   t   splitR   R   t   rstript   allt   npt   arrayt   wheret   difft   anyt   zipt   len(   t   fasta_fnamet   infileR   t   cursorR   t   linet
   line_charst	   n_indicest   gap_maskt	   ok_startst   ok_endst   startt   end(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   get_regions   sH    		c         c   sW   xP t  j |  d    D]9 \ } }  | g  |  D] \ } } } | | f ^ q, f Vq Wd S(   s;   Iterate through BED3 rows: (chrom, BED3-rows-in-this-chrom)c         S   s   |  d S(   Ni    (    (   t   bed3(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   <lambda>P   s    N(   t	   itertoolst   groupby(   t   rowsR   t   _chromR"   R#   (    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   group_regions_by_chromosomeN   s    "c   	   
   c   s  xt  |   D]ÿ \ } } t j d |  t |  } t |  \ } } x´ | D]¬ \ } } | | } | d k s t d | | | | | | f   | | k  rÆ t j d | | | | | |  | } qN t j d | | | |  | | | f V| | } } qN W| | | f Vq Wd S(   s6   Filter regions, joining those separated by small gaps.s   %s: Joining over small gapsi    s/   Impossible gap between %s %d-%d and %d-%d (=%d)s)   	Joining %s %d-%d and %d-%d (gap size %d)s   	Keeping gap %s:%d-%d (size %d)N(   R+   R   R   t   itert   nextt   AssertionError(	   t   regionst   min_gap_sizeR   t   coordst
   prev_startt   prev_endR"   R#   t   gap(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   join_regionsT   s"    
			c      	   c   s"  t  t t |  d t   } t |  d k rI xë | D] } | Vq7 WnÕ xÒ t |  D]Ä \ } } | | k rå t j d |  t | |  } t |  \ } } xw | D]7 \ }	 }
 x( t	 | | |	 |
 | |  D] } | VqÏ Wq§ WqV t j d |  x" | D] \ }	 }
 | |	 |
 f Vqü WqV Wd  S(   Nt
   coord_onlyi    s    %s: Subtracting excluded regionss   %s: No excluded regions(
   t   dictR+   R    t   TrueR   R   R   R,   t   next_or_inft   exclude_in_region(   t	   bed_fnamet   access_rowst   ex_by_chromt   rowR   t   a_rowst   exclude_rowst   ex_startt   ex_endt   a_startt   a_end(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   exclude_regionsl   s"    c         c   só   x" | | k r$ t  |   \ } } q W| | k rB | | | f Vn­ t j d | | | | |  | | k r§ | | k  rï x+ t |  | | | | |  D] } | Vq Wqï nH | | | f V| | k  rï x+ t |  | | | | |  D] } | VqÝ Wn  d S(   sÚ   Take region exclusions from an iterable and apply, perhaps recursively.

    Returns an iterable (usually length 1) of two tuples:
        (accessible chromosome, start, end)
        (current exclusion start, end)
    s4   	Exclusion %s:%d-%d overlaps accessible region %d-%dN(   R9   R   R   R:   (   R@   R   RC   RD   RA   RB   R>   (    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyR:      s     	c         C   s9   y t  |   SWn$ t k
 r4 t d  t d  f SXd  S(   Nt   Inf(   R-   t   StopIterationt   float(   t   iterable(    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyR9   ¦   s    (   t   __doc__R'   R   t   numpyR   t   cnvlib.ngfrillsR    R   R$   R+   R5   RE   R:   R9   (    (    (    sF   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/cnvlib/access.pyt   <module>   s   		5				"