#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'radical.utils==0.7.7','dump_mongodb.py'
__requires__ = 'radical.utils==0.7.7'
import pkg_resources
pkg_resources.run_script('radical.utils==0.7.7', 'dump_mongodb.py')
