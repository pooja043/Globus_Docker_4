#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'khmer==1.1','fastq-to-fasta.py'
__requires__ = 'khmer==1.1'
import pkg_resources
pkg_resources.run_script('khmer==1.1', 'fastq-to-fasta.py')
