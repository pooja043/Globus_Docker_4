ó
_āTc           @   sé   d  Z  d d l Z d d l j Z d d l j Z d d l j j	 Z	 d d l
 j j Z d d l m Z d d  Z d d  Z d d  Z d	   Z d
   Z d   Z d d  Z d   Z d d d d  Z d   Z d   Z d   Z d S(   s/   
Miscellaneous utility code for VAR estimation
i’’’’N(   t   choleskyt   cc         C   s   t  |   } t j g  t | |  D]* } |  | | | !d d d  j   ^ q"  } | d k r t j | d t d | } n  | S(   s²   
    Make predictor matrix for VAR(p) process

    Z := (Z_0, ..., Z_T).T (T x Kp)
    Z_t = [1 y_t y_{t-1} ... y_{t - p + 1}] (Kp x 1)

    Ref: Lutkepohl p.70 (transposed)
    Ni’’’’t   nct   prependt   trend(   t   lent   npt   arrayt   xranget   ravelt   tsat	   add_trendt   True(   t   yt   lagsR   t   nobst   tt   Z(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   get_var_endog   s
    	Ic         C   sX   |  d k r d } n? |  d k r* d } n* |  d k r? d } n |  d k rT d } n  | S(	   NR   i   R   i    t   cti   t   ctti   (    (   R   t
   trendorder(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   get_trendorder"   s    				i   c         C   sī   g  } t  |  t  r! |  g }  n  xi t d | d  D]T } xK |  D]C } t  | t  sf t |  } n  | j d t |  d |  qB Wq5 W| d k r¬ | j d d  n  | d k rĖ | j d d  n  | d k rź | j d d  n  | S(	   sŠ   
    Produce list of lag-variable names. Constant / trends go at the beginning

    Example
    -------
    >>> make_lag_names(['foo', 'bar'], 2, 1)
    ['const', 'L1.foo', 'L1.bar', 'L2.foo', 'L2.bar']

    i   t   Lt   .i    t   constR   i   s   trend**2(   t
   isinstancet
   basestringt   ranget   strt   appendt   insert(   t   namest	   lag_orderR   t	   lag_namest   it   name(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   make_lag_names.   s    
'c         C   s   |  j  \ } } } | | k s$ t  | | } t j | | f  } t j |  d d | | *| d k r d | t j | |  t j | |  f <n  | S(   sā   
    Return compansion matrix for the VAR(1) representation for a VAR(p) process
    (companion form)

    A = [A_1 A_2 ... A_p-1 A_p
         I_K 0       0     0
         0   I_K ... 0     0
         0 ...       I_K   0]
    t   axisi   (   t   shapet   AssertionErrorR   t   zerost   concatenatet   arange(   t   coefst   pt   kt   k2t   kpt   result(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   comp_matrixM   s    

,c         C   s<  d d l  m } d d l m } d d l } d d l j j } d d l } d d l m	 } | j
 | d   } | t |  d   } d }	 x& | d	  | j   k r² |	 d
 7}	 q WxN t r|	 d
 7}	 | j   }
 | j |
  } | r¶ | j   \ } } } Pq¶ q¶ Wt j |  d t d |	 d
 } t |  } t |  } t |  } i | j   | d  6| j   | d  6| j   | d  6} | | } | | d
 } | j | | d
 d
   | } | | } y/ d d l m } | d | d | d |  } Wn9 t k
 r1d d l m } | | d | d | } n X| | f S(   s_   
    Parse data files from Lutkepohl (2005) book

    Source for data files: www.jmulti.de
    i’’’’(   t   deque(   t   datetimeN(   t   asbytess   <(.*) (\w)([\d]+)>.*t   rbi    s   */i   R    t   skip_headert   Qt   Mt   A(   t   DatetimeIndext   startt   freqt   periods(   t	   DateRanget   offset(   t   collectionsR3   R4   t   pandast   pandas.core.datetoolst   coret	   datetoolst   ret   statsmodels.compatnp.py3kR5   t   compilet   opent   popleftR   t   matcht   groupsR   t
   genfromtxtR   t   intt   BQuarterEndt	   BMonthEndt   BYearEndt   rollforwardR;   t   ImportErrorR?   (   t   pathR3   R4   RB   t   dtRF   R5   t   regext   linest   to_skipt   linet   mt   yearR=   t   start_pointt   datat   nt   offsetsR@   t   inct
   start_dateR;   t
   date_rangeR?   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   parse_lutkepohl_datah   sJ    	


c         C   sh   d d l  m } | |   } | d d k r; t d   n) | d d k rZ t d   n
 | d } | S(   Ni’’’’(   t
   np_slogdeti    s   Matrix is not positive definites   Matrix is singulari   (   t   statsmodels.tools.compatibilityRd   t
   ValueError(   RZ   Rd   t   logdet(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt
   get_logdet¤   s    
g©?c         C   s   t  j j d |  d  S(   Ni   i   (   t   statst   normt   ppf(   t   alpha(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   norm_signif_level±   s    c         C   s0   t  j |  d  } |  t  j t  j | |   S(   Ni    (   R   t   diagt   sqrtt   outer(   t   acfRn   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   acf_to_acorrµ   s    id   c         C   sń   | d k	 r" t j j d |  n  d d l m } |  j \ } } } | t j t |   | |  }	 t j | | f  }
 | |	 | |
 | )x^ t	 | |  D]M } |
 | } x: t	 |  D], } | t j
 |  | |
 | | d  7} q¹ Wq W|
 S(   sm   
    Simulate simple VAR(p) process with known coefficients, intercept, white
    noise covariance, etc.
    t   seedi’’’’(   t   multivariate_normali   N(   t   NoneR   t   randomRs   t   numpy.randomRt   R'   R)   R   R   t   dot(   R,   t	   interceptt   sig_ut   stepst
   initvaluesRs   t   rmvnormR-   R.   t   ugenR1   R   t   ygent   j(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   varsim»   s    !
.c         C   sF   y |  j  |  } Wn, t k
 rA t | t  s8   n  | } n X| S(   N(   t   indext	   ExceptionR   RN   (   t   lstR$   R1   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt	   get_indexŠ   s    
c         C   s=   t  j |  d t d t \ } } t j |  } | | | f S(   st   
    Returns
    -------
    W: array of eigenvectors
    eigva: list of eigenvalues
    k: largest eigenvector
    t   leftt   right(   t   decompt   eigR   t   FalseR   t   argmax(   t	   sym_arrayt   eigvat   WR.   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   eigval_decompŁ   s    	!c         C   sw   |  j  d } g  } xN t |  D]@ } | } x1 | | k  r_ | j |  | | f  | d } q/ Wq  Wt j |  } | S(   sp   
    Simple vech operator
    Returns
    -------
    vechvec: vector of all elements on and below diagonal
    i   (   R'   R   R   R   t   asarray(   R:   t   lengtht   vechvecR#   t   b(    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   vechę   s    (   t   __doc__t   numpyR   t   scipy.statsRi   t   scipy.linalgt   linalgR   t   scipy.linalg.decompR   t   statsmodels.tsa.tsatoolsR
   t   tsatoolsR    R   R   R%   R2   Rc   Rh   Rm   Rr   Ru   R   R   R   R   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/statsmodels-0.5.0-py2.7-linux-x86_64.egg/statsmodels/tsa/vector_ar/util.pyt   <module>   s$   		<					