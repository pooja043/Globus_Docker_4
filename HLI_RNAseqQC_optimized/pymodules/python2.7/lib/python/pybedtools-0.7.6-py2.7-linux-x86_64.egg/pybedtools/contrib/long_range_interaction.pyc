ó
dÞáVc           @  s   d  d l  m Z d  d l Z d  d l Z d  d l Z d  d l Z d  d l Z d  d l Z d  d l	 Z	 d  d l
 Z
 e d  Z e d  Z d S(   iÿÿÿÿ(   t   print_functionNc           s£  t  j |   }  |  j   } | d } g  t |  D] } d | ^ q2 } t  j j   } t  j j   } | ry t d  n  d }	 t | d  Ø }
 t | d  À } x¶ t |  j  D]¥ } |	 d 7}	 | j   j	 d    |
 j
 d j   f d   d d d d	 d
 d g | D  d  | j
 d j   f d   d d d d	 d
 d g | D  d  q³ WWd QXWd QXt  j |  } t  j |  } g  g  } } xY | j   D]K \ } } | j |  t | t  j  rÞ| j | j  q | j |  q W| rt d  n  | j t |  d | d t } | r6t d  n  | j t |  d | d t } t j | d     t j | d       f d   } |   |	 | f S(   s5  
    Tag each end of a BEDPE with a set of (possibly many) query BED files.

    For example, given a BEDPE of interacting fragments from a Hi-C experiment,
    identify the contacts between promoters and ChIP-seq peaks. In this case,
    promoters and ChIP-seq peaks of interest would be provided as BED files.

    The strategy is to split the BEDPE into two separate files.  Each file is
    intersected independently with the set of queries.  The results are then
    iterated through in parallel to tie the ends back together. It is this
    iterator that is returned (see example below).

    Parameters
    ----------

    bedpe : str
        BEDPE-format file. Must be name-sorted.

    queries : dict
        Dictionary of BED/GFF/GTF/VCF files to use. After splitting the BEDPE,
        these query files (values in the dictionary) will be passed as the `-b`
        arg to `bedtools intersect`. The keys are passed as the `names`
        argument for `bedtools intersect`.

    Returns
    -------
    Tuple of (iterator, n, extra).

    `iterator` is described below. `n` is the total number of lines in the
    BEDPE file, which is useful for calculating percentage complete for
    downstream work. `extra` is the number of extra fields found in the BEDPE
    (also useful for downstream processing).

    `iterator` yields tuples of (label, end1_hits, end2_hits) where `label` is
    the name field of one line of the original BEDPE file. `end1_hits` and
    `end2_hits` are each iterators of BED-like lines representing all
    identified intersections across all query BED files for end1 and end2 for
    this pair.

    Recall that BEDPE format defines a single name and a single score for each
    pair. For each item in `end1_hits`, the fields are::

        chrom1
        start1
        end1
        name
        score
        strand1
        [extra fields]
        query_label
        fields_from_query_intersecting_end1

    where `[extra fields]` are any additional fields from the original BEDPE,
    `query_label` is one of the keys in the `beds` input dictionary, and the
    remaining fields in the line are the intersecting line from the
    corresponding BED file in the `beds` input dictionary.

    Similarly, each item in `end2_hits` consists of:

        chrom2
        start2
        end2
        name
        score
        strand2
        [extra fields]
        query_label
        fields_from_query_intersecting_end2

    At least one line is reported for every line in the BEDPE file. If there
    was no intersection, the standard BEDTools null fields will be shown. In
    `end1_hits` and `end2_hits`, a line will be reported for each hit in each
    query.

    Example
    -------

    Consider the following BEDPE (where "x1" is an aribtrary extra field).

    >>> bedpe = pybedtools.example_bedtool('test_bedpe.bed')
    >>> print(bedpe) # doctest: +NORMALIZE_WHITESPACE
    chr1  1  10  chr1  50   90   pair1  5  +  -  x1
    chr1  2  15  chr1  200  210  pair2  1  +  +  y1
    <BLANKLINE>


    And the following transcription start sites (TSSes) in BED4 format:

    >>> tsses = pybedtools.example_bedtool('test_tsses.bed')
    >>> print(tsses) # doctest: +NORMALIZE_WHITESPACE
    chr1  5   6   gene1
    chr1  60  61  gene2
    chr1  88  89  gene3
    <BLANKLINE>

    And the following called peaks as BED6:

    >>> peaks = pybedtools.example_bedtool('test_peaks.bed')
    >>> print(peaks) # doctest: +NORMALIZE_WHITESPACE
    chr1  3  4  peak1  50  .
    <BLANKLINE>

    Then we can get the following iterator, n, and extra:

    >>> from pybedtools.contrib.long_range_interaction import tag_bedpe
    >>> iterator, n, extra = tag_bedpe(bedpe, {'tss': tsses, 'pk': peaks})
    >>> print(n)
    2
    >>> print(extra)
    1

    The following illustrates that each item in the iterator represents one
    pair, and each item in each group represents an intersection with one end:

    >>> for (label, end1_hits, end2_hits) in iterator:
    ...    print('PAIR = {}'.format(label))
    ...    print('end1_hits:')
    ...    for i in end1_hits:
    ...        print(i, end='')
    ...    print('end2_hits:')
    ...    for i in end2_hits:
    ...        print(i, end='')  # doctest: +NORMALIZE_WHITESPACE
    PAIR = pair1
    end1_hits:
    chr1  1    10   pair1  5  +  x1  pk   chr1  3   4   peak1  50  .  1
    chr1  1    10   pair1  5  +  x1  tss  chr1  5   6   gene1  1
    end2_hits:
    chr1  50   90   pair1  5  -  x1  tss  chr1  60  61  gene2  1
    chr1  50   90   pair1  5  -  x1  tss  chr1  88  89  gene3  1
    PAIR = pair2
    end1_hits:
    chr1  2    15   pair2  1  +  y1  pk   chr1  3   4   peak1  50  .  1
    chr1  2    15   pair2  1  +  y1  tss  chr1  5   6   gene1  1
    end2_hits:
    chr1  200  210  pair2  1  +  y1  .    -1    -1  .   -1     .  0

    See the `cis_trans_interactions()` function for one way of summarizing
    these data.
    i
   s#   splitting BEDPE into separate filesi    t   wi   s   	c         3  s   |  ] }   | Vq d  S(   N(    (   t   .0t   i(   t   f(    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pys	   <genexpr>±   s    i   i   i   i   s   
c         3  s   |  ] }   | Vq d  S(   N(    (   R   R   (   R   (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pys	   <genexpr>´   s    i   i   i   i	   Ns   intersecting end 1t   namest   waos   intersecting end 2c         S  s   |  d S(   Ni   (    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyt   <lambda>Õ   s    c         S  s   |  d S(   Ni   (    (   R   (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyR   Ö   s    c          3  sS   xL t  j     D]8 \ \ }  } \ } } |  | k s= t  |  | | f Vq Wd  S(   N(   t	   itertoolst   izipt   AssertionError(   t   label1t   group1t   label2t   group2(   t   grouped_end1t   grouped_end2(    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyt   genØ   s    ((   t
   pybedtoolst   BedToolt   field_countt   ranget   _tmpt   printt   opent   fnt   stript   splitt   writet   joint   itemst   appendt
   isinstancet	   intersectt   listt   TrueR   t   groupby(   t   bt   bedst   verboset   observedt   extraR   t
   extra_indst   end1_fnt   end2_fnt   nt   end1_outt   end2_outt   linet   end1_btt   end2_btR   t   fnst   nameR   t	   end1_hitst	   end2_hitsR   (    (   R   R   R   s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyt	   tag_bedpe   sL    
# 
4D!!c      
     s  d } g  } x½|  D]µ\ } } } | d 7} | d d k rn t  d | | t |  d f  t j j   n  t |  } t |  }   f d   }	 t t j t	 t j |	 |    }
 t t j t	 t j |	 |    } xè |
 | f | |
 f g D]Î \ } } x¿ | D]· } | d k r!q	n  t |  j
 | g  } t |  d k rWd g } n  xf | D]^ } xU | D]M } g  } | j |  | j |  | j |  | j |  | j |  qkWq^Wq	Wqö Wq Wt j | d d	 d
 d d d d d g } | j   } | S(   sa  
    Converts the output from `tag_bedpe` into a pandas DataFrame containing
    information about regions that contact each other in cis (same fragment) or
    trans (different fragments).

    For example, given a BEDPE file representing 3D interactions in the genome,
    we want to identify which transcription start sites are connected to distal
    regions containing a peak.

    >>> bedpe = pybedtools.example_bedtool('test_bedpe.bed')
    >>> print(bedpe) # doctest: +NORMALIZE_WHITESPACE
    chr1  1  10  chr1  50   90   pair1  5  +  -  x1
    chr1  2  15  chr1  200  210  pair2  1  +  +  y1
    <BLANKLINE>

    >>> tsses = pybedtools.example_bedtool('test_tsses.bed')
    >>> print(tsses) # doctest: +NORMALIZE_WHITESPACE
    chr1  5   6   gene1
    chr1  60  61  gene2
    chr1  88  89  gene3
    <BLANKLINE>

    >>> peaks = pybedtools.example_bedtool('test_peaks.bed')
    >>> print(peaks) # doctest: +NORMALIZE_WHITESPACE
    chr1  3  4  peak1  50  .
    <BLANKLINE>

    >>> from pybedtools.contrib.long_range_interaction import tag_bedpe
    >>> from pybedtools.contrib.long_range_interaction import cis_tran_interactions
    >>> iterator, n, extra = tag_bedpe(bedpe, {'tss': tsses, 'pk': peaks})

    >>> for (label, group1, group2) in iterator:
    ...    for i in group1:
    ...        print(i, end='')
    ...    for i in group2:
    ...        print(i, end='')  # doctest: +NORMALIZE_WHITESPACE
    chr1  1    10   pair1  5  +  x1  pk   chr1  3   4   peak1  50  .  1
    chr1  1    10   pair1  5  +  x1  tss  chr1  5   6   gene1  1
    chr1  50   90   pair1  5  -  x1  tss  chr1  60  61  gene2  1
    chr1  50   90   pair1  5  -  x1  tss  chr1  88  89  gene3  1
    chr1  2    15   pair2  1  +  y1  pk   chr1  3   4   peak1  50  .  1
    chr1  2    15   pair2  1  +  y1  tss  chr1  5   6   gene1  1
    chr1  200  210  pair2  1  +  y1  .    -1    -1  .   -1     .  0


    >>> iterator, n, extra = tag_bedpe(bedpe, {'tss': tsses, 'pk': peaks})
    >>> df =  cis_trans_interactions(iterator, n, extra)
    >>> print(df)
      target_label target_name cis_label cis_name distal_label distal_name  label
    0          tss       gene1        pk    peak1          tss       gene2  pair1
    1          tss       gene1        pk    peak1          tss       gene3  pair1
    2           pk       peak1       tss    gene1          tss       gene2  pair1
    3           pk       peak1       tss    gene1          tss       gene3  pair1
    4          tss       gene2       tss    gene3          tss       gene1  pair1
    5          tss       gene2       tss    gene3           pk       peak1  pair1
    6          tss       gene3       tss    gene2          tss       gene1  pair1
    7          tss       gene3       tss    gene2           pk       peak1  pair1
    8          tss       gene1        pk    peak1            .           .  pair2
    9           pk       peak1       tss    gene1            .           .  pair2

    If we only care about genes:

    >>> print(df[df.target_label == 'tss'])
      target_label target_name cis_label cis_name distal_label distal_name  label
    0          tss       gene1        pk    peak1          tss       gene2  pair1
    1          tss       gene1        pk    peak1          tss       gene3  pair1
    4          tss       gene2       tss    gene3          tss       gene1  pair1
    5          tss       gene2       tss    gene3           pk       peak1  pair1
    6          tss       gene3       tss    gene2          tss       gene1  pair1
    7          tss       gene3       tss    gene2           pk       peak1  pair1
    8          tss       gene1        pk    peak1            .           .  pair2

    Note that in pair2, there is no evidence of interaction between gene1 and
    gene2.

    What interacts distally with gene2's TSS?

    >>> print(set(df.ix[df.target_name == 'gene2', 'distal_name']).difference('.'))
    set([u'gene1', u'peak1'])

    i    i   iè  s   %d (%.1f%%)id   c           sD   |  d   d k r d St  j |  d    } |  d   | j g S(   s   
            Returns the key (from which file the interval came) and the name
            (of the individual feature).
            i   t   .i   (   R8   R8   (   R   t   create_interval_from_listR4   (   R   t   interval(   R)   (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyt   get_name_hits?  s    R8   t   columnst   target_labelt   target_namet	   cis_labelt   cis_namet   distal_labelt   distal_namet   label(   R8   R8   (   R8   R8   (   R   t   floatt   syst   stdoutt   flushR"   t   setR   t   imapt   tuplet
   differencet   lent   extendR   t   pandast	   DataFramet   drop_duplicates(   t   iteratorR-   R)   R'   t   ct   linesRC   R5   R6   R;   t   names1t   names2t   cist   otherst   targett   non_targetst
   non_targett   otherR0   t   df(    (   R)   s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyt   cis_trans_interactionsá   sF    R
"!!%!(   t
   __future__R    t   osRE   R   t   timet   numpyt   npRN   t   pysamR   t   FalseR7   R#   R]   (    (    (    s   /mnt/galaxyTools/tools/pymodules/python2.7/lib/python/pybedtools-0.7.6-py2.7-linux-x86_64.egg/pybedtools/contrib/long_range_interaction.pyt   <module>   s   Õ