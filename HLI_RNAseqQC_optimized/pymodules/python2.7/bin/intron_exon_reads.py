#!/usr/bin/python2.7
# EASY-INSTALL-SCRIPT: 'pybedtools==0.6.6','intron_exon_reads.py'
__requires__ = 'pybedtools==0.6.6'
import pkg_resources
pkg_resources.run_script('pybedtools==0.6.6', 'intron_exon_reads.py')
